package ru.ckateptb.modpack.core.command;

import com.mojang.brigadier.arguments.ArgumentType;
import com.mojang.brigadier.builder.RequiredArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.suggestion.SuggestionProvider;
import net.minecraft.server.command.CommandManager;
import net.minecraft.server.command.CommandOutput;
import net.minecraft.server.command.CommandSource;
import net.minecraft.server.command.ServerCommandSource;
import ru.ckateptb.modpack.core.mixininterface.CommandOutputHolder;
import ru.ckateptb.modpack.core.permission.interfaces.PermissionHolder;
import ru.ckateptb.modpack.core.util.PermissionHelper;
import ru.ckateptb.sidesplit.Invoke;

public class DynamicCommandArgumentBuilder<S, T> extends AbstractCommandArgumentBuilder<S, T> {
    private final ArgumentType<T> type;
    protected SuggestionProvider<ServerCommandSource> suggestion;

    public DynamicCommandArgumentBuilder(String name, ArgumentType<T> type) {
        super(name);
        this.type = type;
    }

    public CommandArgumentBuilder<S, T> setSuggestions(DynamicSuggestion suggestions) {
        return Invoke.serverValue(() -> {
            this.suggestion = (context, builder) -> CommandSource.suggestMatching(suggestions.getSuggestions(context), builder);
            return this;
        });
    }

    @Override
    public RequiredArgumentBuilder<ServerCommandSource, T> buildThen() {
        return Invoke.serverValue(() -> {
            RequiredArgumentBuilder<ServerCommandSource, T> argumentBuilder = CommandManager.argument(name, type);
            if (this.permission != null && !this.permission.isEmpty()) {
                argumentBuilder = argumentBuilder.requires(sender -> {
                    CommandOutput output = ((CommandOutputHolder) sender).getCommandOutput();
                    return PermissionHelper.hasPermission((PermissionHolder) output, this.permission);
                });
            }
            if (suggestion != null) {
                argumentBuilder = argumentBuilder.suggests(suggestion);
            }
            for (CommandArgumentBuilder<?, ?> child : childList) {
                argumentBuilder = argumentBuilder.then(child.buildThen());
            }
            if (this.executor != null) {
                argumentBuilder.executes(this.executor);
            }
            return argumentBuilder;
        });
    }

    public interface DynamicSuggestion {
        String[] getSuggestions(CommandContext<ServerCommandSource> context);
    }
}
