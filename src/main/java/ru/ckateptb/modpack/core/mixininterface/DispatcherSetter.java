package ru.ckateptb.modpack.core.mixininterface;

import com.mojang.brigadier.CommandDispatcher;
import net.minecraft.server.command.ServerCommandSource;

public interface DispatcherSetter {
    void setDispatcher(CommandDispatcher<ServerCommandSource> dispatcher);
}
