package ru.ckateptb.modpack.core.mixininterface;

import net.minecraft.server.command.CommandOutput;

public interface CommandOutputHolder {
    CommandOutput getCommandOutput();
}
