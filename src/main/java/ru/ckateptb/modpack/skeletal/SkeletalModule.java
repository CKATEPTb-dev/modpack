package ru.ckateptb.modpack.skeletal;

import ru.ckateptb.modpack.core.module.AbstractModule;

/**
 * Модуль для поддержки скелетной анимации IQM
 */
public class SkeletalModule extends AbstractModule {
    private static SkeletalModule instance;

    public SkeletalModule() {
        instance = this;
    }

    public static SkeletalModule getInstance() {
        return instance;
    }
}