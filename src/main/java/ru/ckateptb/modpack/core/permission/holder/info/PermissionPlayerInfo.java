package ru.ckateptb.modpack.core.permission.holder.info;

import ru.ckateptb.modpack.core.permission.PermissionLoader;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class PermissionPlayerInfo {
    private Set<String> groups = Collections.singleton(PermissionLoader.DEFAULT_GROUP_NAME);
    private Set<String> permissions = new HashSet<>();
    private String prefix;
    private String suffix;

    public PermissionPlayerInfo() {
    }

    public Set<String> getGroups() {
        return groups;
    }

    public Set<String> getPermissions() {
        return permissions;
    }

    public String getPrefix() {
        return prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public void addGroup(String group) {
        this.groups.add(group);
    }

    public void removeGroup(String group) {
        this.groups.remove(group);
    }

    public void addPermission(String permission) {
        this.permissions.add(permission);
    }

    public void removePermission(String permission) {
        this.permissions.remove(permission);
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }
}
