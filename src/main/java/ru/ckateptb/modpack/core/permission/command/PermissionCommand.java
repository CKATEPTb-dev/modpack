package ru.ckateptb.modpack.core.permission.command;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.arguments.StringArgumentType;
import net.minecraft.server.PlayerManager;
import net.minecraft.server.command.CommandOutput;
import net.minecraft.text.ClickEvent;
import net.minecraft.text.HoverEvent;
import net.minecraft.text.LiteralText;
import net.minecraft.util.Util;
import ru.ckateptb.modpack.core.chat.message.LiteralTextBuilder;
import ru.ckateptb.modpack.core.command.CommandBuilder;
import ru.ckateptb.modpack.core.command.DynamicCommandArgumentBuilder;
import ru.ckateptb.modpack.core.command.StaticCommandArgumentBuilder;
import ru.ckateptb.modpack.core.mixininterface.OfflinePlayer;
import ru.ckateptb.modpack.core.mixininterface.OfflinePlayersHolder;
import ru.ckateptb.modpack.core.permission.PermissionLoader;
import ru.ckateptb.modpack.core.permission.interfaces.GroupPermissionHolder;
import ru.ckateptb.modpack.core.util.CommandUtil;
import ru.ckateptb.sidesplit.Invoke;

import java.io.IOException;
import java.util.Set;

//TODO
public class PermissionCommand {
    private final HoverEvent moreDetails = new HoverEvent(HoverEvent.Action.SHOW_TEXT, new LiteralText("Нажмите, чтобы получить больше"));

    public PermissionCommand(PermissionLoader loader, PlayerManager playerManager) {
        Invoke.server(() -> new CommandBuilder("permission")
                .addAlias("perm", "pex")
                .setPermission("permission.manager")
                .addChild(new StaticCommandArgumentBuilder<>("reload")
                        .setExecutor(context -> {
                            try {
                                loader.load();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            return Command.SINGLE_SUCCESS;
                        })
                )
                .addChild(new StaticCommandArgumentBuilder<>("player")
                        .addChild(new DynamicCommandArgumentBuilder<>("target", StringArgumentType.word())
                                .setSuggestions(context -> playerManager.getPlayerNames())
                                .addChild(new StaticCommandArgumentBuilder<>("group")
                                        .addChild(new StaticCommandArgumentBuilder<>("list")
                                                .setExecutor(context -> {
                                                    CommandOutput source = CommandUtil.getSource(context);
                                                    String targetName = context.getArgument("target", String.class);
                                                    OfflinePlayer target = ((OfflinePlayersHolder) playerManager).getOfflinePlayer(targetName);
                                                    if (target == null) {
                                                        source.sendSystemMessage(new LiteralText("Неизвестный игрок"), Util.NIL_UUID);
                                                    } else {
                                                        Set<GroupPermissionHolder> groups = target.getGroups();
                                                        groups.stream().map(Object::toString).map(string -> new LiteralTextBuilder("&4&l" + string)
                                                                .setClickEvent(
                                                                        new ClickEvent(
                                                                                ClickEvent.Action.SUGGEST_COMMAND,
                                                                                "/permission group " + string
                                                                        )
                                                                )
                                                                .setHoverEvent(moreDetails)
                                                                .build()
                                                        ).forEach(text -> source.sendSystemMessage(text, Util.NIL_UUID));
                                                    }
                                                    return Command.SINGLE_SUCCESS;
                                                })
                                        )
                                        .addChild(new StaticCommandArgumentBuilder<>("add")
                                                .addChild(new DynamicCommandArgumentBuilder<>("group", StringArgumentType.word())

                                                )
                                        )
                                        .addChild(new StaticCommandArgumentBuilder<>("remove"))
                                )
                                .addChild(new StaticCommandArgumentBuilder<>("permission")
                                        .addChild(new StaticCommandArgumentBuilder<>("list"))
                                        .addChild(new StaticCommandArgumentBuilder<>("add"))
                                        .addChild(new StaticCommandArgumentBuilder<>("remove"))
                                )
                        )
                )
                .addChild(new StaticCommandArgumentBuilder<>("group")
                        .addChild(new DynamicCommandArgumentBuilder<>("target", StringArgumentType.word())
                                .addChild(new StaticCommandArgumentBuilder<>("list"))
                                .addChild(new StaticCommandArgumentBuilder<>("add"))
                                .addChild(new StaticCommandArgumentBuilder<>("remove"))
                        )
                )
                .register());
    }
}
