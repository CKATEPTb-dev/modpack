package ru.ckateptb.sidesplit;

import java.util.concurrent.Callable;

@FunctionalInterface
public interface InvokeClientValue<T> extends Callable<T> {
    T call();
}