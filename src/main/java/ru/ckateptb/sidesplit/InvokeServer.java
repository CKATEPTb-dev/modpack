package ru.ckateptb.sidesplit;

@FunctionalInterface
public interface InvokeServer extends Runnable {
    void run();
}
