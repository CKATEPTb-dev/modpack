package ru.ckateptb.modpack.core.network;

import net.fabricmc.fabric.api.network.PacketContext;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.util.Identifier;

public interface Message {
    Identifier getIdentifier();

    void onReceive(PacketContext packetContext, PacketByteBuf byteBuf);
}
