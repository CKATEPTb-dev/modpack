package ru.ckateptb.modpack.screenshot;

import net.fabricmc.api.EnvType;
import net.minecraft.server.MinecraftServer;
import ru.ckateptb.modpack.core.module.AbstractModule;
import ru.ckateptb.modpack.core.network.NetworkManager;
import ru.ckateptb.modpack.screenshot.command.ScreenshotCommand;
import ru.ckateptb.modpack.screenshot.network.RequestScreenshotMessage;
import ru.ckateptb.modpack.screenshot.network.SendScreenshotMessage;
import ru.ckateptb.sidesplit.Invoke;

/**
 * Шпионский модуль, делает скриншот экрана игрока и отправляет его на IMGUR
 */
public class ScreenshotModule extends AbstractModule {
    public ScreenshotModule() {
        Invoke.client(() -> NetworkManager.register(EnvType.CLIENT, RequestScreenshotMessage.class, RequestScreenshotMessage::new));
        Invoke.server(() -> NetworkManager.register(EnvType.SERVER, SendScreenshotMessage.class, SendScreenshotMessage::new));
    }

    @Override
    public void onServerStarted(MinecraftServer minecraftServer) {
        new ScreenshotCommand();
    }
}
