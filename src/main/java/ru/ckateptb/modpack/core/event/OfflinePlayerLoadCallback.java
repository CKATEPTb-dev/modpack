package ru.ckateptb.modpack.core.event;

import net.fabricmc.fabric.api.event.Event;
import net.fabricmc.fabric.api.event.EventFactory;
import ru.ckateptb.modpack.core.mixininterface.OfflinePlayer;

public interface OfflinePlayerLoadCallback {
    Event<OfflinePlayerLoadCallback> EVENT = EventFactory.createArrayBacked(OfflinePlayerLoadCallback.class, callbacks -> (offlinePlayer) -> {
        for (OfflinePlayerLoadCallback callback : callbacks) {
            callback.onLoad(offlinePlayer);
        }
    });

    void onLoad(OfflinePlayer offlinePlayer);
}
