package ru.ckateptb.modpack.core.mixin;

import net.minecraft.server.command.CommandOutput;
import net.minecraft.server.command.ServerCommandSource;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import ru.ckateptb.modpack.core.mixininterface.CommandOutputHolder;

@Mixin(ServerCommandSource.class)
public abstract class MixinServerCommandSource implements CommandOutputHolder {
    @Shadow
    @Final
    private CommandOutput output;

    @Override
    public CommandOutput getCommandOutput() {
        return output;
    }
}
