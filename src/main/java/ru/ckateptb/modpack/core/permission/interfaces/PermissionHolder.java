package ru.ckateptb.modpack.core.permission.interfaces;

import ru.ckateptb.modpack.core.mixininterface.TagsHolder;

public interface PermissionHolder extends TagsHolder {
    void addPermission(String string);

    void removePermission(String string);

    default boolean hasPermission(String string) {
        return true;
    }
}
