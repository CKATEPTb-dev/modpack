package ru.ckateptb.sidesplit;

import org.spongepowered.asm.mixin.MixinEnvironment;

public class Invoke {
    public static final boolean IS_CLIENT = MixinEnvironment.getCurrentEnvironment().getSide() == MixinEnvironment.Side.CLIENT;
    public static final boolean IS_DEBUG = true;

    public Invoke() {
    }

    public static void client(InvokeClient block) {
        if (IS_CLIENT) block.run();
    }

    public static <T> T clientValue(InvokeClientValue<T> block) {
        return IS_CLIENT ? block.call() : null;
    }

    public static void server(InvokeServer block) {
        block.run();
    }

    public static <T> T serverValue(InvokeServerValue<T> block) {
        return block.call();
    }


    public static void debug(InvokeDebug block) {
        if (IS_DEBUG) block.run();
    }

    public static <T> T debugValue(InvokeDebugValue<T> block) {
        return IS_DEBUG ? block.call() : null;
    }
}
