package ru.ckateptb.modpack.core.mixininterface;

import net.minecraft.server.network.ServerPlayerEntity;
import ru.ckateptb.modpack.core.permission.interfaces.GroupPermissionHolder;
import ru.ckateptb.modpack.core.permission.interfaces.PlayerPermissionHolder;
import ru.ckateptb.modpack.core.permission.node.StringPermissionNode;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;

public interface OfflinePlayer extends PlayerPermissionHolder {

    /**
     * Returns the name of this player
     */
    String getName();

    /**
     * Gets a Player object that this represents, if there is one
     */
    Optional<ServerPlayerEntity> getPlayer();

    /**
     * Returns the UUID of this player
     */
    UUID getUuid();

    /**
     * Checks if this player is currently online
     */
    boolean isOnline();

}
