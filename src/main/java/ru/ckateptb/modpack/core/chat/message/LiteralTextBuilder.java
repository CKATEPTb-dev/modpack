package ru.ckateptb.modpack.core.chat.message;

import net.minecraft.text.ClickEvent;
import net.minecraft.text.HoverEvent;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Style;
import ru.ckateptb.modpack.core.chat.util.ChatUtils;

public class LiteralTextBuilder {
    public static final LiteralText SPACE = new LiteralText(" ");
    public static final LiteralText NEXT_LINE = new LiteralText("\n");
    public static final LiteralText COMMA = new LiteralText(", ");

    private final String text;
    private HoverEvent hoverEvent;
    private ClickEvent clickEvent;

    public LiteralTextBuilder(String text) {
        this.text = text;
    }

    public LiteralTextBuilder setHoverEvent(HoverEvent event) {
        this.hoverEvent = event;
        return this;
    }

    public LiteralTextBuilder setClickEvent(ClickEvent event) {
        this.clickEvent = event;
        return this;
    }

    public LiteralText build() {
        String text = ChatUtils.translateAlternateColorCodes('&', this.text);
        LiteralText literalText = new LiteralText(text);
        Style style = Style.EMPTY;
        if (clickEvent != null) {
            style = style.withClickEvent(clickEvent);
        }
        if (hoverEvent != null) {
            style = style.setHoverEvent(hoverEvent);
        }
        literalText.setStyle(style);
        return literalText;
    }
}
