package ru.ckateptb.modpack.core.player;

import net.minecraft.server.network.ServerPlayerEntity;
import ru.ckateptb.modpack.core.Core;
import ru.ckateptb.modpack.core.event.OfflinePlayerLoadCallback;
import ru.ckateptb.modpack.core.mixininterface.OfflinePlayer;
import ru.ckateptb.modpack.core.permission.holder.PermissionHoldersFactory;
import ru.ckateptb.modpack.core.permission.interfaces.GroupPermissionHolder;
import ru.ckateptb.sidesplit.Invoke;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;

public class ServerOfflinePlayer implements OfflinePlayer {
    private final UUID uuid;
    private final String name;

    public ServerOfflinePlayer(UUID uuid, String name) {
        this.uuid = uuid;
        this.name = name;
        Invoke.server(() -> OfflinePlayerLoadCallback.EVENT.invoker().onLoad(this));
    }

    /**
     * Returns the name of this player
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Gets a Player object that this represents, if there is one
     */
    @Override
    public Optional<ServerPlayerEntity> getPlayer() {
        return Optional.ofNullable(Core.getServer().getPlayerManager().getPlayer(uuid));
    }

    /**
     * Returns the UUID of this player
     */
    @Override
    public UUID getUuid() {
        return uuid;
    }

    /**
     * Checks if this player is currently online
     */
    @Override
    public boolean isOnline() {
        return getPlayer().isPresent();
    }


    @Override
    public boolean hasPermission(String string) {
        return PermissionHoldersFactory.get(uuid).hasPermission(string);
    }

    @Override
    public String getPrefix() {
        return PermissionHoldersFactory.get(uuid).getPrefix();
    }

    @Override
    public String getSuffix() {
        return PermissionHoldersFactory.get(uuid).getSuffix();
    }

    @Override
    public void addPermission(String string) {
        PermissionHoldersFactory.get(uuid).addPermission(string);
    }

    @Override
    public void removePermission(String string) {
        PermissionHoldersFactory.get(uuid).removePermission(string);
    }

    @Override
    public void addGroup(GroupPermissionHolder group) {
        PermissionHoldersFactory.get(uuid).addGroup(group);
    }

    @Override
    public void removeGroup(GroupPermissionHolder group) {
        PermissionHoldersFactory.get(uuid).removeGroup(group);
    }

    @Override
    public Set<GroupPermissionHolder> getGroups() {
        return PermissionHoldersFactory.get(uuid).getGroups();
    }
}
