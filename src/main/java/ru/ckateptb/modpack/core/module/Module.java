package ru.ckateptb.modpack.core.module;

import net.fabricmc.fabric.api.client.event.lifecycle.v1.*;
import net.fabricmc.fabric.api.event.lifecycle.v1.*;

import java.nio.file.Path;

/**
 * Это абстрактный модуль для инициализации других модулей, не трогайте его без необходимости.
 */
public interface Module extends
        ServerChunkEvents.Load,
        ServerChunkEvents.Unload,
        ServerBlockEntityEvents.Load,
        ServerBlockEntityEvents.Unload,
        ServerEntityEvents.Load,
        ServerLifecycleEvents.ServerStarting,
        ServerLifecycleEvents.ServerStarted,
        ServerLifecycleEvents.ServerStopping,
        ServerLifecycleEvents.ServerStopped,
        ServerLifecycleEvents.StartDataPackReload,
        ServerLifecycleEvents.EndDataPackReload,
        ServerTickEvents.StartTick,
        ServerTickEvents.EndTick,
        ServerTickEvents.StartWorldTick,
        ServerTickEvents.EndWorldTick,
        ClientBlockEntityEvents.Load,
        ClientBlockEntityEvents.Unload,
        ClientChunkEvents.Load,
        ClientChunkEvents.Unload,
        ClientEntityEvents.Load,
        ClientEntityEvents.Unload,
        ClientLifecycleEvents.ClientStarted,
        ClientLifecycleEvents.ClientStopping,
        ClientTickEvents.StartTick,
        ClientTickEvents.EndTick,
        ClientTickEvents.StartWorldTick,
        ClientTickEvents.EndWorldTick {
    Path getModuleFolder();
}
