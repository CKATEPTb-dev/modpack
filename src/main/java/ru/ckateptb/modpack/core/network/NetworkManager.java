package ru.ckateptb.modpack.core.network;

import net.fabricmc.api.EnvType;
import net.fabricmc.fabric.api.network.ClientSidePacketRegistry;
import net.fabricmc.fabric.api.network.PacketConsumer;
import net.fabricmc.fabric.api.network.PacketRegistry;
import net.fabricmc.fabric.api.network.ServerSidePacketRegistry;
import net.minecraft.util.Identifier;
import org.spongepowered.asm.mixin.transformer.throwables.IllegalClassLoadError;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.function.Supplier;

public class NetworkManager {
    public static final ClientSidePacketRegistry CLIENT = ClientSidePacketRegistry.INSTANCE;
    public static final ServerSidePacketRegistry SERVER = ServerSidePacketRegistry.INSTANCE;
    private static final String MISSING_IDENTIFIER = "Message {%s} don't contains static field with type Identifier";

    public static void register(EnvType type, Class<? extends Message> msgClass, Supplier<? extends Message> message) {
        Identifier identifier = getIdentifier(msgClass);
        if (identifier != null) {
            PacketConsumer consumer = (context, byteBuf) -> {
                Message emptyInstance = message.get();
                MessageSerializer.readObject(emptyInstance, byteBuf);
                emptyInstance.onReceive(context, byteBuf);
            };
            PacketRegistry registry = type == EnvType.CLIENT ? CLIENT : SERVER;
            registry.register(identifier, consumer);
        }
    }

    private static Identifier getIdentifier(Class<? extends Message> message) {
        try {
            for (Field field : message.getFields()) {
                if (Modifier.isStatic(field.getModifiers())) {
                    if (field.getType() == Identifier.class) {
                        return (Identifier) field.get(null);
                    }
                }
            }
            throw new IllegalClassLoadError(String.format(MISSING_IDENTIFIER, message.toString()));
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }
}