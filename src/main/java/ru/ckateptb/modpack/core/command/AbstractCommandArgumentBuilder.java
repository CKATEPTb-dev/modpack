package ru.ckateptb.modpack.core.command;

import com.mojang.brigadier.Command;
import net.minecraft.server.command.ServerCommandSource;
import ru.ckateptb.sidesplit.Invoke;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractCommandArgumentBuilder<S, T> implements CommandArgumentBuilder<S, T> {
    protected final List<CommandArgumentBuilder<?, ?>> childList = new ArrayList<>();
    protected final String name;
    protected String permission;
    protected Command<ServerCommandSource> executor;

    public AbstractCommandArgumentBuilder(String name) {
        this.name = name;
    }


    public CommandArgumentBuilder<S, T> setPermission(String permission) {
        return Invoke.serverValue(() -> {
            this.permission = permission;
            return this;
        });
    }

    public CommandArgumentBuilder<S, T> addChild(CommandArgumentBuilder<?, ?> child) {
        return Invoke.serverValue(() -> {
            childList.add(child);
            return this;
        });
    }

    public CommandArgumentBuilder<S, T> setExecutor(Command<ServerCommandSource> executor) {
        return Invoke.serverValue(() -> {
            this.executor = executor;
            return this;
        });
    }
}
