package ru.ckateptb.modpack.core.network;

import io.netty.buffer.Unpooled;
import net.fabricmc.fabric.api.network.ClientSidePacketRegistry;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.network.packet.c2s.play.CustomPayloadC2SPacket;

public interface ServerMessage extends Message {
    default CustomPayloadC2SPacket getPacket() {
        PacketByteBuf buf = new PacketByteBuf(Unpooled.buffer());
        MessageSerializer.writeObject(this, buf);
        return new CustomPayloadC2SPacket(getIdentifier(), buf);
    }

    default void sendToServer() {
        ClientSidePacketRegistry.INSTANCE.sendToServer(getPacket());
    }
}
