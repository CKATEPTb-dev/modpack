package ru.ckateptb.sidesplit;

@FunctionalInterface
public interface InvokeDebug extends Runnable {
    void run();
}
