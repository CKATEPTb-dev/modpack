package ru.ckateptb.modpack.core.permission.holder;

import ru.ckateptb.modpack.core.permission.holder.info.PermissionGroupInfo;
import ru.ckateptb.modpack.core.permission.holder.info.PermissionPlayerInfo;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PermissionHoldersFactory {
    private static final Map<String, Map.Entry<PermissionGroup, PermissionGroupInfo>> GROUPS = new HashMap<>();
    private static final Map<UUID, Map.Entry<PermissionPlayer, PermissionPlayerInfo>> PLAYERS = new HashMap<>();

    public static void load(Map<String, PermissionGroupInfo> groups, Map<String, PermissionPlayerInfo> players) {
        GROUPS.clear();
        PLAYERS.clear();
        groups.forEach((name, group) -> GROUPS.put(name, unmodifiableEntry(new PermissionGroup(name, group), group)));
        players.forEach((stringUuid, player) -> {
            UUID uuid = UUID.fromString(stringUuid);
            PLAYERS.put(uuid, unmodifiableEntry(new PermissionPlayer(uuid, player), player));
        });
    }

    public static PermissionGroup get(String name) {
        return GROUPS.get(name).getKey();
    }

    public static PermissionPlayer get(UUID uuid) {
        return PLAYERS.get(uuid).getKey();
    }

    public static PermissionGroupInfo info(String name) {
        return GROUPS.get(name).getValue();
    }

    public static PermissionPlayerInfo info(UUID uuid) {
        return PLAYERS.get(uuid).getValue();
    }

    private static <K, V> Map.Entry<K, V> unmodifiableEntry(K key, V value) {
        return new Map.Entry<K, V>() {
            @Override
            public K getKey() {
                return key;
            }

            @Override
            public V getValue() {
                return value;
            }

            @Override
            public V setValue(V value) {
                return value;
            }
        };
    }
}
