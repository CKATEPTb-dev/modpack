package ru.ckateptb.modpack.core.chat.channel;

import net.minecraft.server.network.ServerPlayerEntity;

public interface ChatChannel {
    String getName();

    void sendMessage(ServerPlayerEntity entity, String message);
}
