package ru.ckateptb.modpack.core.chat.util;

import net.minecraft.server.command.CommandOutput;
import net.minecraft.text.Text;
import net.minecraft.util.Util;
import org.apache.commons.lang3.Validate;
import ru.ckateptb.modpack.core.chat.message.LiteralTextBuilder;

public class ChatUtils {
    public static String translateAlternateColorCodes(char altColorChar, String textToTranslate) {
        Validate.notNull(textToTranslate, "Cannot translate null text");
        char[] b = textToTranslate.toCharArray();
        for (int i = 0; i < b.length - 1; i++) {
            if (b[i] == altColorChar && "0123456789AaBbCcDdEeFfKkLlMmNnOoRrXx".indexOf(b[i + 1]) > -1) {
                b[i] = '\u00A7';
                b[i + 1] = Character.toLowerCase(b[i + 1]);
            }
        }
        return new String(b);
    }

    public static void sendMessage(CommandOutput to, Text message) {
        to.sendSystemMessage(message, Util.NIL_UUID);
    }

    public static void sendMessage(CommandOutput to, String message) {
        to.sendSystemMessage(new LiteralTextBuilder("&7&l".concat(message)).build(), Util.NIL_UUID);
    }

}
