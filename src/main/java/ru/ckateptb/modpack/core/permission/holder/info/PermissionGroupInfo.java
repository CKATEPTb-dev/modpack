package ru.ckateptb.modpack.core.permission.holder.info;

import java.util.HashSet;
import java.util.Set;

public class PermissionGroupInfo {
    private Set<String> permissions = new HashSet<>();
    private String prefix = "prefix";
    private String suffix = "suffix";

    public PermissionGroupInfo() {
    }

    public Set<String> getPermissions() {
        return permissions;
    }

    public String getPrefix() {
        return prefix;
    }

    public String getSuffix() {
        return suffix;
    }
}
