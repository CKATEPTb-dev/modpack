package ru.ckateptb.sidesplit;

@FunctionalInterface
public interface InvokeClient extends Runnable {
    void run();
}
