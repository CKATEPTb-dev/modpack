package ru.zaxar163.gradle.task;

import org.tomlj.Toml;
import org.tomlj.TomlArray;
import org.tomlj.TomlParseResult;
import org.tomlj.TomlVersion;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Config {
    public final List<Pattern> clientExclusions;
    public final List<Pattern> serverExclusions;
    public final List<Pattern> bothExclusions;
    public final List<Pattern> transformerExclusions;

    public Config(File f) throws IOException {
        TomlParseResult result = Toml.parse(f.toPath(), TomlVersion.V0_5_0);
        clientExclusions = mapArray(result.getArrayOrEmpty("client"));
        serverExclusions = mapArray(result.getArrayOrEmpty("server"));
        bothExclusions = mapArray(result.getArrayOrEmpty("both"));
        transformerExclusions = mapArray(result.getArrayOrEmpty("transformerExclusions"));
    }

    public Config() {
        clientExclusions = serverExclusions = bothExclusions = transformerExclusions = Collections.emptyList();
    }

    private static List<Pattern> mapArray(TomlArray arr) {
        return Collections.unmodifiableList(arr.toList().stream().filter(e -> e instanceof String)
                .map(e -> Pattern.compile(e.toString())).collect(Collectors.toList()));
    }
}
