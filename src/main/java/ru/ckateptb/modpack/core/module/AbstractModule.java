package ru.ckateptb.modpack.core.module;

import net.minecraft.block.entity.BlockEntity;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.entity.Entity;
import net.minecraft.resource.ServerResourceManager;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.WorldSavePath;
import net.minecraft.world.chunk.WorldChunk;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.ckateptb.modpack.core.Core;
import ru.ckateptb.sidesplit.Invoke;

import java.nio.file.Path;

public abstract class AbstractModule implements Module {
    private static final Logger logger = LogManager.getLogger();
    private final WorldSavePath worldSavePath;

    public AbstractModule() {
        String name = this.getClass().getSimpleName();
        this.worldSavePath = new WorldSavePath(name.toLowerCase());
        Invoke.debug(() -> logger.log(Level.INFO, "Loading module: " + name));
    }

    @Override
    public void onLoad(BlockEntity blockEntity, ClientWorld clientWorld) {

    }

    @Override
    public void onUnload(BlockEntity blockEntity, ClientWorld clientWorld) {

    }

    @Override
    public void onChunkLoad(ClientWorld clientWorld, WorldChunk worldChunk) {

    }

    @Override
    public void onChunkUnload(ClientWorld clientWorld, WorldChunk worldChunk) {

    }

    @Override
    public void onLoad(Entity entity, ClientWorld clientWorld) {

    }

    @Override
    public void onUnload(Entity entity, ClientWorld clientWorld) {

    }

    @Override
    public void onClientStarted(MinecraftClient minecraftClient) {

    }

    @Override
    public void onClientStopping(MinecraftClient minecraftClient) {

    }

    @Override
    public void onEndTick(MinecraftClient minecraftClient) {

    }

    @Override
    public void onEndTick(ClientWorld clientWorld) {

    }

    @Override
    public void onStartTick(MinecraftClient minecraftClient) {

    }

    @Override
    public void onStartTick(ClientWorld clientWorld) {

    }

    @Override
    public void onLoad(BlockEntity blockEntity, ServerWorld serverWorld) {

    }

    @Override
    public void onUnload(BlockEntity blockEntity, ServerWorld serverWorld) {

    }

    @Override
    public void onChunkLoad(ServerWorld serverWorld, WorldChunk worldChunk) {

    }

    @Override
    public void onChunkUnload(ServerWorld serverWorld, WorldChunk worldChunk) {

    }

    @Override
    public void onLoad(Entity entity, ServerWorld serverWorld) {

    }

    @Override
    public void endDataPackReload(MinecraftServer minecraftServer, ServerResourceManager serverResourceManager, boolean b) {

    }

    @Override
    public void onServerStarted(MinecraftServer minecraftServer) {

    }

    @Override
    public void onServerStarting(MinecraftServer minecraftServer) {

    }

    @Override
    public void onServerStopped(MinecraftServer minecraftServer) {

    }

    @Override
    public void onServerStopping(MinecraftServer minecraftServer) {

    }

    @Override
    public void startDataPackReload(MinecraftServer minecraftServer, ServerResourceManager serverResourceManager) {

    }

    @Override
    public void onEndTick(MinecraftServer minecraftServer) {
    }

    @Override
    public void onEndTick(ServerWorld serverWorld) {

    }

    @Override
    public void onStartTick(MinecraftServer minecraftServer) {

    }

    @Override
    public void onStartTick(ServerWorld serverWorld) {

    }

    @Override
    public Path getModuleFolder() {
        return Invoke.serverValue(() -> Core.getServer().getSavePath(worldSavePath));
    }
}
