package ru.ckateptb.modpack.core.tree.child;

import java.util.HashMap;
import java.util.Map;

public abstract class NamedTreeChild<V> implements TreeChild<String, V> {
    private final Map<String, V> childMap = new HashMap<>();

    @Override
    public V addChild(String key, V value) {
        return childMap.putIfAbsent(key, value);
    }

    @Override
    public V getChild(String key) {
        return childMap.get(key);
    }
}
