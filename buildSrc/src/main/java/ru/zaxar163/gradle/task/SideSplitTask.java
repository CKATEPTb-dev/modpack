package ru.zaxar163.gradle.task;

import org.gradle.api.DefaultTask;
import org.gradle.api.logging.LogLevel;
import org.gradle.api.tasks.InputFile;
import org.gradle.api.tasks.Optional;
import org.gradle.api.tasks.OutputFile;
import org.gradle.api.tasks.TaskAction;
import org.objectweb.asm.*;
import org.objectweb.asm.tree.*;

import java.io.*;
import java.nio.file.Files;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class SideSplitTask extends DefaultTask {
    // Constants
    private static final String SIDEONLY_DESK = "Lnet/fabricmc/api/Environment;";
    private static final String INVOKE_CORE = "ru/ckateptb/sidesplit/Invoke";
    private static final String INVOKEVALUE_METHOD = "call()Ljava/lang/Object;";
    private static final String INVOKE_METHOD = "run()V";
    @InputFile
    private File input;
    @InputFile
    @Optional
    private File config = null;
    private File taskDir = new File(getProject().getBuildDir(), getName());
    @OutputFile
    private File classesServer = new File(taskDir, "classes_server.jar");
    @OutputFile
    private File classesClient = new File(taskDir, "classes_client.jar");

    private static boolean checkMatches(List<Pattern> toCheck, String name) {
        return toCheck.stream().anyMatch(e -> e.matcher(name).matches());
    }

    private static ZipEntry newZipEntry(String e) {
        ZipEntry ret = new ZipEntry(e);
        ret.setTime(0);
        return ret;
    }

    private static void transfer(InputStream input, OutputStream output) throws IOException {
        byte[] buffer = new byte[4096];
        for (int length = input.read(buffer); length >= 0; length = input.read(buffer)) {
            output.write(buffer, 0, length);
        }
    }

    public File getInput() {
        return input;
    }

    public void setInput(File input) {
        this.input = input;
    }

    public File getConfig() {
        return config;
    }

    public void setConfig(File config) {
        this.config = config;
    }

    public File getServerClasses() {
        return classesServer;
    }

    public File getClientClasses() {
        return classesClient;
    }

    @TaskAction
    public void doAction() throws IOException, InterruptedException {
        Config c = config != null ? new Config(config) : new Config();
        ExecutorService exec = new ThreadPoolExecutor(0, 4,
                40L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>());
        if (!Files.exists(taskDir.toPath())) Files.createDirectory(taskDir.toPath());
        try (ZipInputStream in = new ZipInputStream(new FileInputStream(input));
             ZipOutputStream server = new ZipOutputStream((new FileOutputStream(classesServer)));
             ZipOutputStream client = new ZipOutputStream((new FileOutputStream(classesClient)))) {
            BlockingQueue<ClassData> serverData = new LinkedBlockingQueue<>();
            BlockingQueue<ClassData> clientData = new LinkedBlockingQueue<>();
            Thread[] writer = {new Thread(() -> {
                while (!(exec.isTerminated() && clientData.isEmpty())) {
                    try {
                        ClassData d = clientData.poll(20, TimeUnit.MILLISECONDS);
                        if (d == null) continue;
                        client.putNextEntry(d.entry);
                        client.write(d.classData);
                    } catch (InterruptedException e) {
                        return;
                    } catch (IOException e) {
                        this.getLogger().log(LogLevel.ERROR, "Error on writing (client)", e);
                    }
                }
            }, "ZipWriterClient"),
                    new Thread(() -> {
                        while (!(exec.isTerminated() && serverData.isEmpty())) {
                            try {
                                ClassData d = serverData.poll(20, TimeUnit.MILLISECONDS);
                                if (d == null) continue;
                                server.putNextEntry(d.entry);
                                server.write(d.classData);
                            } catch (InterruptedException e) {
                                return;
                            } catch (IOException e) {
                                this.getLogger().log(LogLevel.ERROR, "Error on writing (server)", e);
                            }
                        }
                    }, "ZipWriterServer")};
            for (Thread t : writer) t.start();
            ZipEntry e = in.getNextEntry();
            while (e != null) {
                if (e.isDirectory() || e.getName() == null || e.getName().isEmpty()) {
                    e = in.getNextEntry();
                    continue;
                }
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                transfer(in, out);
                byte[] cls = out.toByteArray();
                final String name = e.getName();
                exec.submit(() -> {
                    if (checkMatches(c.bothExclusions, name)) return;
                    if (!name.endsWith(".class") || checkMatches(c.transformerExclusions, name)) {
                        if (!checkMatches(c.serverExclusions, name))
                            serverData.add(new ClassData(cls, newZipEntry(name)));
                        if (!checkMatches(c.clientExclusions, name))
                            clientData.add(new ClassData(cls, newZipEntry(name)));
                    } else {
                        ClassNode classNode = new ClassNode();
                        ClassReader classReader = new ClassReader(cls);
                        classReader.accept(classNode, ClassReader.EXPAND_FRAMES);
                        removeLambdas(classNode, Side.DEBUG, this.getLogger());
                        ClassNode serverCls = new ClassNode();
                        classNode.accept(serverCls);
                        if (processClass(serverCls, Side.SERVER) && !checkMatches(c.serverExclusions, name)) {
                            removeLambdas(serverCls, Side.CLIENT, this.getLogger());
                            ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_MAXS);
                            serverCls.accept(cw);
                            serverData.add(new ClassData(cw.toByteArray(), newZipEntry(name)));
                        }
                        ClassNode clientCls = new ClassNode();
                        classNode.accept(clientCls);
                        if (processClass(clientCls, Side.CLIENT) && !checkMatches(c.clientExclusions, name)) {
                            removeLambdas(clientCls, Side.SERVER, this.getLogger());
                            ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_MAXS);
                            clientCls.accept(cw);
                            clientData.add(new ClassData(cw.toByteArray(), newZipEntry(name)));
                        }
                    }
                });
                e = in.getNextEntry();
            }
            exec.shutdown();
            exec.awaitTermination(2, TimeUnit.MINUTES);
            for (Thread t : writer) t.join();
        }
    }

    private static boolean processClass(ClassNode classNode, Side side) {
        if (remove(classNode.visibleAnnotations, side.name)
            || remove(classNode.invisibleAnnotations, side.name))
            return false;
        classNode.fields.removeIf(field -> remove(field.visibleAnnotations, side.name) || remove(field.invisibleAnnotations, side.name));
        classNode.methods.removeIf(method -> remove(method.visibleAnnotations, side.name) || remove(method.invisibleAnnotations, side.name));
        return true;
    }

    private static void removeLambdas(ClassNode classNode, Side opposite, org.gradle.api.logging.Logger l) {
        boolean remove = classNode.interfaces.stream().anyMatch(opposite.invokeInterfaceName::equals);
        boolean removeValue = classNode.interfaces.stream().anyMatch(opposite.invokeValueInterfaceName::equals);
        Set<String> forRemove = new HashSet<>();
        for (MethodNode method : classNode.methods) {
            if (remove && INVOKE_METHOD.equals(method.name + method.desc)) {
                method.instructions.clear();
                method.instructions.add(new InsnNode(Opcodes.RET));
                continue;
            }
            if (removeValue && INVOKEVALUE_METHOD.equals(method.name + method.desc)) {
                method.instructions.clear();
                method.instructions.add(new InsnNode(Opcodes.ACONST_NULL));
                method.instructions.add(new InsnNode(Opcodes.ARETURN));
                continue;
            }
            Arrays.stream(method.instructions.toArray()).filter(e -> e instanceof MethodInsnNode)
                    .map(e -> (MethodInsnNode) e).forEach(e -> {
                if (INVOKE_CORE.equals(e.owner) &&
                        (opposite.invokeName.equals(e.name) || opposite.invokeValueName.equals(e.name))) {
                    InsnList lst = new InsnList();
                    lst.add(new InsnNode(Opcodes.POP));
                    if (opposite.invokeValueName.equals(e.name))
                        lst.add(new InsnNode(Opcodes.ACONST_NULL));
                    method.instructions.insert(e, lst);
                    method.instructions.remove(e);
                }
            });
            Arrays.stream(method.instructions.toArray()).filter(a -> a instanceof InvokeDynamicInsnNode)
                    .map(e -> (InvokeDynamicInsnNode) e).filter(e -> "java/lang/invoke/LambdaMetafactory".equals(e.bsm.getOwner()) &&
                    "metafactory".equals(e.bsm.getName())).forEach(e -> {
                String internalInterfaceName = Type.getMethodType(e.desc).getReturnType().getInternalName();
                Handle methodCaller = (Handle) e.bsmArgs[1];
                if (opposite.invokeInterfaceName.equals(internalInterfaceName) ||
                        opposite.invokeValueInterfaceName.equals(internalInterfaceName)) {
                    if (!classNode.name.equals(methodCaller.getOwner())) {
                        l.log(LogLevel.WARN,
                                "Class calls not its class lambda which should be removed: " + classNode.name + " in " + method.name);
                        return;
                    }
                    if (!methodCaller.getName().startsWith("lambda$")) return;
                    forRemove.add(methodCaller.getName() + methodCaller.getDesc());
                    method.instructions.insert(e, new InsnNode(Opcodes.ACONST_NULL));
                    method.instructions.remove(e);
                }
            });
        }
        Set<String> forRemoveBackup = new HashSet<>(forRemove);
        classNode.methods.stream().filter(e -> forRemoveBackup.contains(e.name + e.desc))
                .map(e -> e.instructions.toArray()).forEach(inst -> parseLambdaMethod(inst, classNode, forRemove));
        classNode.methods.removeIf(e -> forRemove.contains(e.name + e.desc));
    }

    private static void parseLambdaMethod(AbstractInsnNode[] inst, ClassNode classNode, Set<String> forRemove) {
        Arrays.stream(inst).filter(a -> a instanceof InvokeDynamicInsnNode)
                .map(e -> (InvokeDynamicInsnNode) e).filter(e -> "java/lang/invoke/LambdaMetafactory".equals(e.bsm.getOwner()) &&
                "metafactory".equals(e.bsm.getName())).filter(e ->
                classNode.name.equals(((Handle) e.bsmArgs[1]).getOwner())).filter(
                e -> ((Handle) e.bsmArgs[1]).getName().startsWith("lambda$")).forEach(e -> {
            Handle h = (Handle) e.bsmArgs[1];
            MethodNode f = null;
            for (MethodNode m : classNode.methods) {
                if (m.desc.equals(h.getDesc()) && m.name.equals(h.getName())) {
                    f = m;
                    break;
                }
            }
            if (f != null) {
                forRemove.add(h.getName() + h.getDesc());
                parseLambdaMethod(f.instructions.toArray(), classNode, forRemove);
            }
        });
    }

    private static boolean remove(List<AnnotationNode> anns, String side) {
        if (anns == null)
            return false;
        for (AnnotationNode ann : anns) {
            if (SIDEONLY_DESK.equals(ann.desc) && ann.values != null) {
                for (int x = 0; x < ann.values.size() - 1; x += 2) {
                    Object key = ann.values.get(x);
                    Object value = ann.values.get(x + 1);
                    if (key.equals("value") && value instanceof String[] && !((String[]) value)[1].equals(side))
                        return true;
                }
            }
        }
        return false;
    }

    private static class ClassData {
        private final byte[] classData;
        private final ZipEntry entry;

        private ClassData(byte[] classData, ZipEntry entry) {
            this.classData = classData;
            this.entry = entry;
        }
    }
}
