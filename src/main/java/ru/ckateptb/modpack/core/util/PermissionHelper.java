package ru.ckateptb.modpack.core.util;

import ru.ckateptb.modpack.core.permission.interfaces.PermissionHolder;

public class PermissionHelper {
    public static boolean hasPermission(PermissionHolder output, String permission) {
        return output.hasPermission(permission);
    }
}
