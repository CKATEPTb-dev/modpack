package ru.ckateptb.modpack.core.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

public class IOHelper {
    public static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
    public static final Charset UNICODE_CHARSET = StandardCharsets.UTF_8;
    private static final OpenOption[] WRITE_OPTIONS = {StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING};
    private static final OpenOption[] APPEND_OPTIONS = {StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.APPEND};

    public static boolean exists(Path path) {
        return Files.exists(path);
    }

    public static BufferedReader newReader(Path file) throws IOException {
        return Files.newBufferedReader(file, UNICODE_CHARSET);
    }

    public static BufferedWriter newWriter(Path file) throws IOException {
        return newWriter(file, false);
    }

    public static BufferedWriter newWriter(Path file, boolean append) throws IOException {
        createParentDirs(file);
        return Files.newBufferedWriter(file, UNICODE_CHARSET, append ? APPEND_OPTIONS : WRITE_OPTIONS);
    }

    public static void createParentDirs(Path path) throws IOException {
        Path parent = path.getParent();
        if (parent != null && !isDir(parent))
            Files.createDirectories(parent);
    }

    public static boolean isDir(Path path) {
        return Files.isDirectory(path);
    }

    public static boolean isFile(Path path) {
        return Files.isRegularFile(path);
    }

}
