package ru.ckateptb.modpack.core.mixin;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.GameProfileRepository;
import net.minecraft.util.UserCache;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;

import javax.annotation.Nullable;
import java.util.Deque;
import java.util.Locale;
import java.util.Map;

@Mixin(UserCache.class)
public abstract class MixinUserCache {
    @Shadow
    @Final
    private Deque<GameProfile> byAccessTime;

    @Shadow
    @Final
    private GameProfileRepository profileRepository;

    @Shadow
    @Final
    private Map<String, UserCache.Entry> byName;

    @Shadow
    private static GameProfile findProfileByName(GameProfileRepository repository, String name) {
        return null;
    }

    @Shadow
    public abstract void add(GameProfile gameProfile);

    @Shadow
    public abstract void save();

    /**
     * @author CKATEPTb
     * @reason
     */
    @Nullable
    @Overwrite
    public GameProfile findByName(String string) {
        String string2 = string.toLowerCase(Locale.ROOT);
        UserCache.Entry entry = this.byName.get(string2);
        GameProfile gameProfile2;
        if (entry != null) {
            gameProfile2 = entry.getProfile();
            this.byAccessTime.remove(gameProfile2);
            this.byAccessTime.addFirst(gameProfile2);
        } else {
            gameProfile2 = findProfileByName(this.profileRepository, string2);
            if (gameProfile2 != null) {
                this.add(gameProfile2);
                entry = this.byName.get(string2);
            }
        }
        this.save();
        return entry == null ? null : entry.getProfile();
    }

}
