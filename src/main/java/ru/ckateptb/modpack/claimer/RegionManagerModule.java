package ru.ckateptb.modpack.claimer;

import ru.ckateptb.modpack.core.module.AbstractModule;

/**
 * Этот модуль выступает аналогом WorldGuard
 */
public class RegionManagerModule extends AbstractModule {
    private static RegionManagerModule instance;

    public RegionManagerModule() {
        instance = this;
    }

    public static RegionManagerModule getInstance() {
        return instance;
    }
}