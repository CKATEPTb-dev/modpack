package ru.ckateptb.modpack.core.mixin;

import com.mojang.authlib.GameProfile;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.screen.ScreenHandlerListener;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import ru.ckateptb.modpack.core.mixininterface.OfflinePlayer;
import ru.ckateptb.modpack.core.mixininterface.OfflinePlayersHolder;
import ru.ckateptb.modpack.core.mixininterface.TagsHolder;
import ru.ckateptb.modpack.core.permission.interfaces.GroupPermissionHolder;
import ru.ckateptb.modpack.core.permission.interfaces.PlayerPermissionHolder;

import java.util.Set;

@Mixin(ServerPlayerEntity.class)
public abstract class MixinServerPlayerEntity extends PlayerEntity implements ScreenHandlerListener, PlayerPermissionHolder, TagsHolder {

    @Shadow
    @Final
    public MinecraftServer server;
    private OfflinePlayer offlinePlayer;

    public MixinServerPlayerEntity(World world, BlockPos blockPos, GameProfile gameProfile) {
        super(world, blockPos, gameProfile);
    }

    @Inject(method = "writeCustomDataToTag", at = @At("RETURN"))
    public void toTag(CompoundTag tag, CallbackInfo info) {
        tag.putString("username", getEntityName());
    }

    private OfflinePlayer getOfflinePlayer() {
        if (offlinePlayer == null) {
            offlinePlayer = ((OfflinePlayersHolder) server.getPlayerManager()).getOfflinePlayer(uuid);
        }
        return offlinePlayer;
    }

    @Override
    public Set<GroupPermissionHolder> getGroups() {
        return getOfflinePlayer().getGroups();
    }

    @Override
    public void addGroup(GroupPermissionHolder group) {
        getOfflinePlayer().addGroup(group);
    }

    @Override
    public void removeGroup(GroupPermissionHolder group) {
        getOfflinePlayer().removeGroup(group);
    }

    @Override
    public void addPermission(String perm) {
        getOfflinePlayer().addPermission(perm);
    }

    @Override
    public void removePermission(String perm) {
        getOfflinePlayer().removePermission(perm);
    }

    @Override
    public boolean hasPermission(String string) {
        return getOfflinePlayer().hasPermission(string);
    }

    @Override
    public String getPrefix() {
        return getOfflinePlayer().getPrefix();
    }

    @Override
    public String getSuffix() {
        return getOfflinePlayer().getSuffix();
    }
}
