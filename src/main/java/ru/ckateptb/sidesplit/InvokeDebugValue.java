package ru.ckateptb.sidesplit;

import java.util.concurrent.Callable;

@FunctionalInterface
public interface InvokeDebugValue<T> extends Callable<T> {
    T call();
}
