package ru.ckateptb.modpack.core.mixininterface;

import net.minecraft.util.Util;
import ru.ckateptb.modpack.core.permission.interfaces.PermissionHolder;

import java.util.UUID;

public interface UuidHolder extends PermissionHolder {
    default UUID getUuid() {
        return Util.NIL_UUID;
    }
}
