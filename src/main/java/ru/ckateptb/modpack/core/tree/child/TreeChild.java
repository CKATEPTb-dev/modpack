package ru.ckateptb.modpack.core.tree.child;

public interface TreeChild<K, V> {
    V addChild(K key, V value);

    V getChild(K key);
}
