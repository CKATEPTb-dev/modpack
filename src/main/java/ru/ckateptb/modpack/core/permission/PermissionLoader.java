package ru.ckateptb.modpack.core.permission;

import com.google.gson.reflect.TypeToken;
import ru.ckateptb.modpack.core.Core;
import ru.ckateptb.modpack.core.event.OfflinePlayerLoadCallback;
import ru.ckateptb.modpack.core.mixininterface.OfflinePlayer;
import ru.ckateptb.modpack.core.mixininterface.OfflinePlayersHolder;
import ru.ckateptb.modpack.core.permission.holder.PermissionHoldersFactory;
import ru.ckateptb.modpack.core.permission.holder.info.PermissionGroupInfo;
import ru.ckateptb.modpack.core.permission.holder.info.PermissionPlayerInfo;
import ru.ckateptb.modpack.core.util.IOHelper;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static ru.ckateptb.modpack.core.util.IOHelper.GSON;

public class PermissionLoader {
    public static final TypeToken<HashMap<String, PermissionGroupInfo>> GROUP_TOKEN = new TypeToken<HashMap<String, PermissionGroupInfo>>() {
    };
    public static final TypeToken<HashMap<String, PermissionPlayerInfo>> PLAYER_TOKEN = new TypeToken<HashMap<String, PermissionPlayerInfo>>() {
    };
    public static final String DEFAULT_GROUP_NAME = "default";

    private final Path groupsFile;
    private Map<String, PermissionGroupInfo> groupMap = Collections.singletonMap(DEFAULT_GROUP_NAME, new PermissionGroupInfo());
    private final Path playersFile;
    private Map<String, PermissionPlayerInfo> playerMap = new HashMap<>();

    public PermissionLoader(Path dir) {
        this.groupsFile = dir.resolve("groups.json");
        this.playersFile = dir.resolve("players.json");
        OfflinePlayerLoadCallback.EVENT.register(offlinePlayer -> {
            loadPlayer(offlinePlayer.getUuid().toString());
            try {
                savePlayers();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void load() throws IOException {
        PermissionHoldersFactory.load(loadGroups(), loadPlayers());
    }

    private Map<String, PermissionGroupInfo> loadGroups() throws IOException {
        if (!Files.exists(groupsFile)) {
            saveGroups();
        }
        try (BufferedReader reader = IOHelper.newReader(groupsFile)) {
            groupMap = GSON.fromJson(reader, GROUP_TOKEN.getType());
        }
        return groupMap;
    }

    private Map<String, PermissionPlayerInfo> loadPlayers() throws IOException {
        if (!Files.exists(playersFile)) {
            savePlayers();
        }
        try (BufferedReader reader = IOHelper.newReader(playersFile)) {
            playerMap = GSON.fromJson(reader, PLAYER_TOKEN.getType());
        }
        ((OfflinePlayersHolder) Core.getServer().getPlayerManager()).getOfflinePlayerList().stream()
                .map(OfflinePlayer::getUuid).map(Objects::toString).filter(e -> !playerMap.containsKey(e))
                .forEach(this::loadPlayer);
        savePlayers();
        return playerMap;
    }

    private void loadPlayer(String uuid) {
        if (!playerMap.containsKey(uuid)) {
            playerMap.put(uuid, new PermissionPlayerInfo());
        }
    }

    public void saveGroups() throws IOException {
        try (BufferedWriter writer = IOHelper.newWriter(groupsFile)) {
            GSON.toJson(groupMap, GROUP_TOKEN.getType(), writer);
        }
    }

    public void savePlayers() throws IOException {
        try (BufferedWriter writer = IOHelper.newWriter(playersFile)) {
            GSON.toJson(playerMap, PLAYER_TOKEN.getType(), writer);
        }
    }
}
