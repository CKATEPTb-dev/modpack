package ru.ckateptb.modpack.screenshot.command;

import com.mojang.brigadier.Command;
import net.minecraft.command.arguments.EntityArgumentType;
import net.minecraft.server.command.CommandOutput;
import net.minecraft.server.network.ServerPlayerEntity;
import ru.ckateptb.modpack.core.command.CommandBuilder;
import ru.ckateptb.modpack.core.command.DynamicCommandArgumentBuilder;
import ru.ckateptb.modpack.core.mixininterface.UuidHolder;
import ru.ckateptb.modpack.core.permission.interfaces.PermissionHolder;
import ru.ckateptb.modpack.core.util.CommandUtil;
import ru.ckateptb.modpack.screenshot.network.RequestScreenshotMessage;
import ru.ckateptb.sidesplit.Invoke;

public class ScreenshotCommand {
    public ScreenshotCommand() {
        Invoke.server(() -> new CommandBuilder("screenshot")
                .setPermission("screenshot.request")
                .addChild(new DynamicCommandArgumentBuilder<>("target", EntityArgumentType.player())
                        .setExecutor(context -> {
                            ServerPlayerEntity target = EntityArgumentType.getPlayer(context, "target");
                            CommandOutput source = CommandUtil.getSource(context);
                            new RequestScreenshotMessage(((UuidHolder) source).getUuid(), "c45e31f498a183d").sendToPlayer(target);
                            return Command.SINGLE_SUCCESS;
                        })
                )
                .register());
    }
}
