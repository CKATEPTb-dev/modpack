package ru.ckateptb.modpack.core.permission.interfaces;

public interface GroupPermissionHolder extends PermissionHolder {
    String getName();
}
