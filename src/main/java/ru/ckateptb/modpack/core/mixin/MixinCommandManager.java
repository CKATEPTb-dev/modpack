package ru.ckateptb.modpack.core.mixin;

import com.mojang.brigadier.CommandDispatcher;
import net.minecraft.server.command.CommandManager;
import net.minecraft.server.command.ServerCommandSource;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Mutable;
import org.spongepowered.asm.mixin.Shadow;
import ru.ckateptb.modpack.core.mixininterface.DispatcherSetter;

@Mixin(CommandManager.class)
public class MixinCommandManager implements DispatcherSetter {

    @Mutable
    @Shadow
    @Final
    private CommandDispatcher<ServerCommandSource> dispatcher;

    public void setDispatcher(CommandDispatcher<ServerCommandSource> dispatcher) {
        this.dispatcher = dispatcher;
    }
}
