package ru.ckateptb.modpack.core;

import com.mojang.brigadier.CommandDispatcher;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.PlayerManager;
import net.minecraft.server.command.ServerCommandSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.ckateptb.modpack.core.mixininterface.DispatcherSetter;
import ru.ckateptb.modpack.core.mixininterface.OfflinePlayersHolder;
import ru.ckateptb.modpack.core.module.AbstractModule;
import ru.ckateptb.modpack.core.permission.PermissionLoader;
import ru.ckateptb.modpack.core.permission.command.PermissionCommand;
import ru.ckateptb.sidesplit.Invoke;

import java.io.IOException;

/**
 * Этот модуль выступает в качестве API, чтобы не плодить тонны кода.
 * все общие утилиты, билдеры и другие вспомогательные классы должны быть тут.
 */
public class Core extends AbstractModule {
    private static final Logger LOGGER = LogManager.getLogger();
    @Environment(EnvType.SERVER)
    private static MinecraftServer server;

    public static MinecraftServer getServer() {
        return server;
    }

    @Override
    public void onServerStarting(MinecraftServer minecraftServer) {
        Invoke.server(() -> {
            server = minecraftServer;
            replaceCommandDispatcher();
        });
    }

    @Override
    public void onServerStarted(MinecraftServer minecraftServer) {
        Invoke.server(() -> {
            try {
                PlayerManager playerManager = server.getPlayerManager();
                ((OfflinePlayersHolder) playerManager).loadFromPlayerDataFolder();
                PermissionLoader permissionLoader = new PermissionLoader(getModuleFolder().resolve("permission"));
                permissionLoader.load();
                new PermissionCommand(permissionLoader, playerManager);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Убираем нахуй обычные команды майна, ибо мне это говно не нужно!
     */
    private void replaceCommandDispatcher() {
        Invoke.server(() -> {
            CommandDispatcher<ServerCommandSource> dispatcher = new CommandDispatcher<>();
            dispatcher.findAmbiguities((commandNode, commandNode2, commandNode3, collection) ->
                    LOGGER.warn("Ambiguity between arguments {} and {} with inputs: {}",
                            dispatcher.getPath(commandNode2), dispatcher.getPath(commandNode3), collection));
            dispatcher.setConsumer((commandContext, bl, i) ->
                    commandContext.getSource().onCommandComplete(commandContext, bl, i));
            ((DispatcherSetter) server.getCommandManager()).setDispatcher(dispatcher);
        });
    }
}
