package ru.ckateptb.modpack.core.permission.holder;

import ru.ckateptb.modpack.core.permission.holder.info.PermissionPlayerInfo;
import ru.ckateptb.modpack.core.permission.interfaces.GroupPermissionHolder;
import ru.ckateptb.modpack.core.permission.interfaces.PlayerPermissionHolder;
import ru.ckateptb.modpack.core.permission.node.StringPermissionNode;
import ru.ckateptb.sidesplit.Invoke;

import java.util.*;

public class PermissionPlayer implements PlayerPermissionHolder {
    private final Set<GroupPermissionHolder> groups = new HashSet<>();
    private final StringPermissionNode permissions = new StringPermissionNode();
    private final String prefix;
    private final String suffix;
    private final UUID uuid;

    public PermissionPlayer(UUID uuid, PermissionPlayerInfo info) {
        this.uuid = uuid;
        this.prefix = info.getPrefix();
        this.suffix = info.getSuffix();
        for (String group : info.getGroups()) {
            groups.add(PermissionHoldersFactory.get(group));
        }
        for (String permission : info.getPermissions()) {
            StringPermissionNode permissionNode = this.permissions;
            for (String node : permission.split("\\.")) {
                permissionNode = permissionNode.addChild(node);
            }
        }
    }

    @Override
    public boolean hasPermission(String permission) {
        return Invoke.serverValue(() -> {
            StringPermissionNode permissionNode = this.permissions;
            for (String node : permission.split("\\.")) {
                if (permissionNode.getChild("*").isPresent()) {
                    return true;
                }
                permissionNode = permissionNode.getChild(node);
            }
            boolean has = permissionNode.isPresent();
            Iterator<GroupPermissionHolder> it = groups.iterator();
            while (it.hasNext() && !has) {
                has = it.next().hasPermission(permission);
            }
            return has;
        });
    }


    @Override
    public String getPrefix() {
        return prefix;
    }

    @Override
    public String getSuffix() {
        return suffix;
    }

    public UUID getUuid() {
        return uuid;
    }

    @Override
    public void addGroup(GroupPermissionHolder group) {
        //TODO
    }

    @Override
    public void removeGroup(GroupPermissionHolder group) {
        //TODO
    }

    @Override
    public void addPermission(String string) {
        //TODO
    }

    @Override
    public void removePermission(String string) {
        //TODO
    }

    @Override
    public Set<GroupPermissionHolder> getGroups() {
        return Collections.unmodifiableSet(groups);
    }
}
