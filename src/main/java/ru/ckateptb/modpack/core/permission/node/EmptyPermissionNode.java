package ru.ckateptb.modpack.core.permission.node;

public class EmptyPermissionNode extends StringPermissionNode {
    public static EmptyPermissionNode INSTANCE = new EmptyPermissionNode();

    @Override
    public StringPermissionNode addChild(String name) {
        return this;
    }

    @Override
    public StringPermissionNode getChild(String name) {
        return this;
    }

    @Override
    public boolean isPresent() {
        return false;
    }
}
