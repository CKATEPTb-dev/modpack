package ru.ckateptb.modpack.core.command;

import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import net.minecraft.server.command.CommandManager;
import net.minecraft.server.command.CommandOutput;
import net.minecraft.server.command.ServerCommandSource;
import ru.ckateptb.modpack.core.mixininterface.CommandOutputHolder;
import ru.ckateptb.modpack.core.permission.interfaces.PermissionHolder;
import ru.ckateptb.modpack.core.util.PermissionHelper;
import ru.ckateptb.sidesplit.Invoke;

public class StaticCommandArgumentBuilder<S> extends AbstractCommandArgumentBuilder<S, LiteralArgumentBuilder<S>> {
    public StaticCommandArgumentBuilder(String name) {
        super(name);
    }

    @Override
    public LiteralArgumentBuilder<ServerCommandSource> buildThen() {
        return Invoke.serverValue(() -> {
            LiteralArgumentBuilder<ServerCommandSource> argumentBuilder = CommandManager.literal(name);
            if (this.permission != null && !this.permission.isEmpty()) {
                argumentBuilder = argumentBuilder.requires(sender -> {
                    CommandOutput output = ((CommandOutputHolder) sender).getCommandOutput();
                    return PermissionHelper.hasPermission((PermissionHolder) output, this.permission);
                });
            }
            for (CommandArgumentBuilder<?, ?> child : childList) {
                argumentBuilder = argumentBuilder.then(child.buildThen());
            }
            if (this.executor != null) {
                argumentBuilder.executes(this.executor);
            }
            return argumentBuilder;
        });
    }
}
