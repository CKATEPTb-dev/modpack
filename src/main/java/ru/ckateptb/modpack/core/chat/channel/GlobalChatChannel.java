package ru.ckateptb.modpack.core.chat.channel;

import net.minecraft.server.MinecraftServer;
import net.minecraft.server.PlayerManager;
import net.minecraft.server.network.ServerPlayerEntity;

public class GlobalChatChannel extends AbstractChatChannel {

    protected GlobalChatChannel(String name, MinecraftServer server, PlayerManager playerManager) {
        super(name, server, playerManager);
    }

    @Override
    public void sendMessage(ServerPlayerEntity entity, String message) {

    }
}
