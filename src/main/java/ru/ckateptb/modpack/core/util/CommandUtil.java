package ru.ckateptb.modpack.core.util;

import com.mojang.brigadier.context.CommandContext;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.command.CommandOutput;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.Util;
import ru.ckateptb.modpack.core.Core;
import ru.ckateptb.modpack.core.mixininterface.CommandOutputHolder;
import ru.ckateptb.sidesplit.Invoke;

import java.util.UUID;

public class CommandUtil {
    public static CommandOutput getSource(CommandContext<ServerCommandSource> context) {
        return ((CommandOutputHolder) context.getSource()).getCommandOutput();
    }

    public static CommandOutput getSource(UUID uuid) {
        return Invoke.serverValue(() -> {
            MinecraftServer server = Core.getServer();
            if (uuid.equals(Util.NIL_UUID)) {
                return server;
            }
            ServerPlayerEntity serverPlayerEntity = server.getPlayerManager().getPlayer(uuid);
            return serverPlayerEntity == null ? server : serverPlayerEntity;
        });
    }
}
