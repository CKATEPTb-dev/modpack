package ru.ckateptb.modpack.core.mixininterface;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

public interface OfflinePlayersHolder {

    List<OfflinePlayer> getOfflinePlayerList();

    @Nullable
    OfflinePlayer getOfflinePlayer(UUID uuid);

    @Nullable
    OfflinePlayer getOfflinePlayer(String name);

    void loadFromPlayerDataFolder() throws IOException;
}
