package ru.ckateptb.modpack.core.permission.node;

import ru.ckateptb.modpack.core.tree.child.NamedTreeChild;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class StringPermissionNode extends NamedTreeChild<PermissionNode> implements PermissionNode {
    private final Map<String, StringPermissionNode> childNodes = new ConcurrentHashMap<>();

    public StringPermissionNode addChild(String name) {
        name = name.toLowerCase();
        if (childNodes.containsKey(name)) {
            return childNodes.get(name);
        }
        StringPermissionNode child = new StringPermissionNode();
        childNodes.put(name, child);
        return child;
    }

    public StringPermissionNode getChild(String name) {
        return childNodes.getOrDefault(name.toLowerCase(), EmptyPermissionNode.INSTANCE);
    }

    @Override
    public boolean isPresent() {
        return true;
    }
}
