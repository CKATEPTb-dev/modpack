package ru.ckateptb.modpack.core.command;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import net.minecraft.server.command.CommandManager;
import net.minecraft.server.command.CommandOutput;
import net.minecraft.server.command.ServerCommandSource;
import org.apache.commons.lang3.Validate;
import ru.ckateptb.modpack.core.Core;
import ru.ckateptb.modpack.core.mixininterface.CommandOutputHolder;
import ru.ckateptb.modpack.core.permission.interfaces.PermissionHolder;
import ru.ckateptb.modpack.core.util.PermissionHelper;
import ru.ckateptb.sidesplit.Invoke;

import java.util.*;

public class CommandBuilder {
    private final String name;
    private final Set<String> aliases = new HashSet<>();
    private final List<CommandArgumentBuilder<?, ?>> arguments = new ArrayList<>();
    private String permission;
    private Command<ServerCommandSource> executor;

    public CommandBuilder(String name) {
        this.name = name;
    }

    public CommandBuilder addAlias(String... alias) {
        return Invoke.serverValue(() -> {
            this.aliases.addAll(Arrays.asList(alias));
            return this;
        });
    }

    public CommandBuilder setPermission(String permission) {
        return Invoke.serverValue(() -> {
            this.permission = permission;
            return this;
        });
    }

    public CommandBuilder setExecutor(Command<ServerCommandSource> executor) {
        return Invoke.serverValue(() -> {
            this.executor = executor;
            return this;
        });
    }

    public <S, T> CommandBuilder addChild(CommandArgumentBuilder<S, T> argumentBuilder) {
        return Invoke.serverValue(() -> {
            this.arguments.add(argumentBuilder);
            return this;
        });
    }

    public LiteralArgumentBuilder<ServerCommandSource> build() {
        return Invoke.serverValue(() -> build(name));
    }

    private LiteralArgumentBuilder<ServerCommandSource> build(String name) {
        return Invoke.serverValue(() -> {
            Validate.notNull(name, "Name can't be null");
            LiteralArgumentBuilder<ServerCommandSource> command = CommandManager.literal(name);
            if (this.permission != null && !this.permission.isEmpty()) {
                command = command.requires(sender -> {
                    CommandOutput output = ((CommandOutputHolder) sender).getCommandOutput();
                    return PermissionHelper.hasPermission((PermissionHolder) output, this.permission);
                });
            }
            for (CommandArgumentBuilder<?, ?> argument : arguments) {
                command = command.then(argument.buildThen());
            }
            if (this.executor != null) {
                command = command.executes(this.executor);
            }
            return command;
        });
    }

    public void register() {
        Invoke.server(() -> {
            Validate.notNull(name, "Name can't be null");
            aliases.add(name);
            aliases.forEach(name -> Core.getServer().getCommandManager().getDispatcher().register(build(name)));
        });
    }
}
