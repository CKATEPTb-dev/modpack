package ru.ckateptb.modpack.core.permission.holder;

import ru.ckateptb.modpack.core.permission.holder.info.PermissionGroupInfo;
import ru.ckateptb.modpack.core.permission.interfaces.GroupPermissionHolder;
import ru.ckateptb.modpack.core.permission.node.StringPermissionNode;

public class PermissionGroup implements GroupPermissionHolder {
    private final StringPermissionNode permissions = new StringPermissionNode();
    private final String name;
    private final String prefix;
    private final String suffix;

    public PermissionGroup(String name, PermissionGroupInfo info) {
        this.name = name;
        this.prefix = info.getPrefix();
        this.suffix = info.getSuffix();
        for (String permission : info.getPermissions()) {
            StringPermissionNode permissionNode = this.permissions;
            for (String node : permission.split("\\.")) {
                permissionNode = permissionNode.addChild(node);
            }
        }
    }

    @Override
    public boolean hasPermission(String permission) {
        StringPermissionNode permissionNode = this.permissions;
        for (String node : permission.split("\\.")) {
            if (permissionNode.getChild("*").isPresent()) {
                return true;
            }
            permissionNode = permissionNode.getChild(node);
        }
        return permissionNode.isPresent();
    }

    @Override
    public void addPermission(String string) {
        //TODO
    }

    @Override
    public void removePermission(String string) {
        //TODO
    }

    @Override
    public String getPrefix() {
        return prefix;
    }

    @Override
    public String getSuffix() {
        return suffix;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
}
