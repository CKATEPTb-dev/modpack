package ru.ckateptb.modpack;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.*;
import net.fabricmc.fabric.api.event.lifecycle.v1.*;
import ru.ckateptb.modpack.claimer.RegionManagerModule;
import ru.ckateptb.modpack.core.Core;
import ru.ckateptb.modpack.core.module.Module;
import ru.ckateptb.modpack.essential.EssentialModule;
import ru.ckateptb.modpack.screenshot.ScreenshotModule;
import ru.ckateptb.modpack.skeletal.SkeletalModule;
import ru.ckateptb.sidesplit.Invoke;

import java.util.ArrayList;
import java.util.List;

/**
 * Это главный модуль, который собирает все ваши модули воедино.
 * Чтобы добавить новый модуль, внесите его в registerModules,
 * а так-же в build.gradle (см. как внесены другие модули и делайте так-же)
 */
public class ModPack implements ModInitializer {
    private final List<Module> modules = new ArrayList<>();

    private void registerModules() {
        modules.add(new Core());
        modules.add(new EssentialModule());
        modules.add(new RegionManagerModule());
        modules.add(new ScreenshotModule());
        modules.add(new SkeletalModule());
    }

    @Override
    public void onInitialize() {
        registerModules();
        Invoke.server(() -> {
            ServerBlockEntityEvents.BLOCK_ENTITY_LOAD.register((blockEntity, serverWorld) -> modules
                    .forEach(module -> module.onLoad(blockEntity, serverWorld)));
            ServerBlockEntityEvents.BLOCK_ENTITY_UNLOAD.register((blockEntity, serverWorld) -> modules
                    .forEach(module -> module.onUnload(blockEntity, serverWorld)));
            ServerChunkEvents.CHUNK_LOAD.register((serverWorld, worldChunk) -> modules
                    .forEach(module -> module.onChunkLoad(serverWorld, worldChunk)));
            ServerChunkEvents.CHUNK_UNLOAD.register((serverWorld, worldChunk) -> modules
                    .forEach(module -> module.onChunkUnload(serverWorld, worldChunk)));
            ServerEntityEvents.ENTITY_LOAD.register((entity, serverWorld) -> modules
                    .forEach(module -> module.onLoad(entity, serverWorld)));
            ServerLifecycleEvents.START_DATA_PACK_RELOAD.register((minecraftServer, serverResourceManager) -> modules
                    .forEach(module -> module.startDataPackReload(minecraftServer, serverResourceManager)));
            ServerLifecycleEvents.END_DATA_PACK_RELOAD.register((minecraftServer, serverResourceManager, b) -> modules
                    .forEach(module -> module.endDataPackReload(minecraftServer, serverResourceManager, b)));
            ServerLifecycleEvents.SERVER_STARTED.register(minecraftServer -> modules
                    .forEach(module -> module.onServerStarted(minecraftServer)));
            ServerLifecycleEvents.SERVER_STOPPED.register(minecraftServer -> modules
                    .forEach(module -> module.onServerStopped(minecraftServer)));
            ServerLifecycleEvents.SERVER_STARTING.register(minecraftServer -> modules
                    .forEach(module -> module.onServerStarting(minecraftServer)));
            ServerLifecycleEvents.SERVER_STOPPING.register(minecraftServer -> modules
                    .forEach(module -> module.onServerStopping(minecraftServer)));
            ServerTickEvents.START_SERVER_TICK.register(minecraftServer -> modules
                    .forEach(module -> module.onStartTick(minecraftServer)));
            ServerTickEvents.END_SERVER_TICK.register(minecraftServer -> modules
                    .forEach(module -> module.onEndTick(minecraftServer)));
            ServerTickEvents.START_WORLD_TICK.register(serverWorld -> modules
                    .forEach(module -> module.onStartTick(serverWorld)));
            ServerTickEvents.END_WORLD_TICK.register(serverWorld -> modules
                    .forEach(module -> module.onEndTick(serverWorld)));
        });
        Invoke.client(() -> {
            ClientBlockEntityEvents.BLOCK_ENTITY_LOAD.register((blockEntity, clientWorld) -> modules
                    .forEach(module -> module.onLoad(blockEntity, clientWorld)));
            ClientBlockEntityEvents.BLOCK_ENTITY_UNLOAD.register((blockEntity, clientWorld) -> modules
                    .forEach(module -> module.onUnload(blockEntity, clientWorld)));
            ClientChunkEvents.CHUNK_LOAD.register((clientWorld, worldChunk) -> modules
                    .forEach(module -> module.onChunkLoad(clientWorld, worldChunk)));
            ClientChunkEvents.CHUNK_UNLOAD.register((clientWorld, worldChunk) -> modules
                    .forEach(module -> module.onChunkUnload(clientWorld, worldChunk)));
            ClientEntityEvents.ENTITY_LOAD.register((entity, clientWorld) -> modules
                    .forEach(module -> module.onLoad(entity, clientWorld)));
            ClientEntityEvents.ENTITY_UNLOAD.register((entity, clientWorld) -> modules
                    .forEach(module -> module.onUnload(entity, clientWorld)));
            ClientLifecycleEvents.CLIENT_STARTED.register(minecraftClient -> modules
                    .forEach(module -> module.onClientStarted(minecraftClient)));
            ClientLifecycleEvents.CLIENT_STOPPING.register(minecraftClient -> modules
                    .forEach(module -> module.onClientStopping(minecraftClient)));
            ClientTickEvents.END_CLIENT_TICK.register(minecraftClient -> modules
                    .forEach(module -> module.onEndTick(minecraftClient)));
            ClientTickEvents.START_CLIENT_TICK.register(minecraftClient -> modules
                    .forEach(module -> module.onStartTick(minecraftClient)));
            ClientTickEvents.END_WORLD_TICK.register(clientWorld -> modules
                    .forEach(module -> module.onEndTick(clientWorld)));
            ClientTickEvents.START_WORLD_TICK.register(clientWorld -> modules
                    .forEach(module -> module.onStartTick(clientWorld)));
        });
    }
}

