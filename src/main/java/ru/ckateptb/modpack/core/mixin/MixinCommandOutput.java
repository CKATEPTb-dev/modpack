package ru.ckateptb.modpack.core.mixin;

import net.minecraft.server.command.CommandOutput;
import org.spongepowered.asm.mixin.Mixin;
import ru.ckateptb.modpack.core.mixininterface.UuidHolder;

@Mixin(CommandOutput.class)
public interface MixinCommandOutput extends UuidHolder {
}
