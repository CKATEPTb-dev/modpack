package ru.ckateptb.sidesplit;

import java.util.concurrent.Callable;

@FunctionalInterface
public interface InvokeServerValue<T> extends Callable<T> {
    T call();
}
