package ru.ckateptb.modpack.essential;

import net.minecraft.server.MinecraftServer;
import ru.ckateptb.modpack.core.command.CommandBuilder;
import ru.ckateptb.modpack.core.module.AbstractModule;

/**
 * Этот модуль выступает аналогом EssentialsX
 */
public class EssentialModule extends AbstractModule {
    private static EssentialModule instance;

    public EssentialModule() {
        instance = this;
    }

    public static EssentialModule getInstance() {
        return instance;
    }

    @Override
    public void onServerStarted(MinecraftServer minecraftServer) {
        CommandBuilder clear = new CommandBuilder("clear");
        clear.setPermission("essentials.clear");
        clear.setExecutor(context -> {
            return 1;
        });
    }
}
