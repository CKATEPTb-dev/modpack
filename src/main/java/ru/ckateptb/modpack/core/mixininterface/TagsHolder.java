package ru.ckateptb.modpack.core.mixininterface;

public interface TagsHolder {
    String getPrefix();

    String getSuffix();
}
