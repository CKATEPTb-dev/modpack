package ru.ckateptb.modpack.core.chat.channel;

import net.minecraft.server.MinecraftServer;
import net.minecraft.server.PlayerManager;

public abstract class AbstractChatChannel implements ChatChannel {
    protected final MinecraftServer server;
    protected final PlayerManager playerManager;
    private final String name;

    protected AbstractChatChannel(String name, MinecraftServer server, PlayerManager playerManager) {
        this.name = name;
        this.server = server;
        this.playerManager = playerManager;
    }

    @Override
    public String getName() {
        return name;
    }
}
