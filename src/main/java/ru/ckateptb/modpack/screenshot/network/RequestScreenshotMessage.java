package ru.ckateptb.modpack.screenshot.network;

import net.fabricmc.fabric.api.network.PacketContext;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gl.Framebuffer;
import net.minecraft.client.texture.NativeImage;
import net.minecraft.client.util.ScreenshotUtils;
import net.minecraft.client.util.Window;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.util.Identifier;
import ru.ckateptb.modpack.core.network.ClientMessage;
import ru.ckateptb.modpack.screenshot.util.ImgurUtil;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.UUID;

public class RequestScreenshotMessage implements ClientMessage {
    public static final Identifier IDENTIFIER = new Identifier("screenshot", "request");
    public UUID uuid;
    public String id;

    public RequestScreenshotMessage() {
    }

    public RequestScreenshotMessage(UUID uuid, String id) {
        this.uuid = uuid;
        this.id = id;
    }

    @Override
    public void onReceive(PacketContext context, PacketByteBuf byteBuf) {
        context.getTaskQueue().execute(() -> {
            MinecraftClient minecraft = MinecraftClient.getInstance();
            Window mainWindow = minecraft.getWindow();
            int width = mainWindow.getFramebufferWidth();
            int height = mainWindow.getFramebufferHeight();
            Framebuffer framebuffer = minecraft.getFramebuffer();
            NativeImage image = ScreenshotUtils.takeScreenshot(width, height, framebuffer);
            new Thread(() -> {
                try {
                    File file = Files.createTempFile("screenshot", ".png").toFile();
                    image.writeFile(file);
                    String screenshot = ImgurUtil.uploadToImgur(file, id);
                    new SendScreenshotMessage(uuid, screenshot).sendToServer();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }, "Screenshot Saving Thread").start();
        });
    }

    @Override
    public Identifier getIdentifier() {
        return IDENTIFIER;
    }
}
