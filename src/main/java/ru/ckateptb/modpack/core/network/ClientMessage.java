package ru.ckateptb.modpack.core.network;

import io.netty.buffer.Unpooled;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.network.packet.s2c.play.CustomPayloadS2CPacket;
import net.minecraft.server.network.ServerPlayerEntity;
import ru.ckateptb.modpack.core.Core;

public interface ClientMessage extends Message {
    default CustomPayloadS2CPacket getPacket() {
        PacketByteBuf buf = new PacketByteBuf(Unpooled.buffer());
        MessageSerializer.writeObject(this, buf);
        return new CustomPayloadS2CPacket(getIdentifier(), buf);
    }

    default void sendToPlayer(ServerPlayerEntity entity) {
        entity.networkHandler.sendPacket(getPacket());
    }

    default void sendToPlayers(ServerPlayerEntity... entities) {
        for (ServerPlayerEntity entity : entities) {
            sendToPlayer(entity);
        }
    }

    default void sendToAll() {
        Core.getServer().getPlayerManager().sendToAll(getPacket());
    }
}
