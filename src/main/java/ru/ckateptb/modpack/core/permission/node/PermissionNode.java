package ru.ckateptb.modpack.core.permission.node;

public interface PermissionNode {
    boolean isPresent();
}
