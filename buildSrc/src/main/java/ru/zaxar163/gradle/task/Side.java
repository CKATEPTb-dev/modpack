package ru.zaxar163.gradle.task;

public enum Side {
    CLIENT("CLIENT", "client", "clientValue",
            "ru/ckateptb/sidesplit/InvokeClient",
            "ru/ckateptb/sidesplit/InvokeClientValue"),
    SERVER("SERVER", "server", "serverValue",
            "ru/ckateptb/sidesplit/InvokeServer",
            "ru/ckateptb/sidesplit/InvokeServerValue"),
    DEBUG("DEBUG", "debug", "debugValue",
            "ru/ckateptb/sidesplit/InvokeDebug",
            "ru/ckateptb/sidesplit/InvokeDebugValue");
    public final String name;
    public final String invokeName;
    public final String invokeValueName;
    public final String invokeInterfaceName;
    public final String invokeValueInterfaceName;

    Side(String name, String invokeName, String invokeValueName, String invokeInterfaceName, String invokeValueInterfaceName) {
        this.name = name;
        this.invokeName = invokeName;
        this.invokeValueName = invokeValueName;
        this.invokeInterfaceName = invokeInterfaceName;
        this.invokeValueInterfaceName = invokeValueInterfaceName;
    }
}
