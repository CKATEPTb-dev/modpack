package ru.ckateptb.modpack.screenshot.network;

import net.fabricmc.fabric.api.network.PacketContext;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.command.CommandOutput;
import net.minecraft.text.ClickEvent;
import net.minecraft.util.Identifier;
import ru.ckateptb.modpack.core.chat.message.LiteralTextBuilder;
import ru.ckateptb.modpack.core.chat.util.ChatUtils;
import ru.ckateptb.modpack.core.network.ServerMessage;
import ru.ckateptb.modpack.core.util.CommandUtil;
import ru.ckateptb.sidesplit.Invoke;

import java.util.UUID;

public class SendScreenshotMessage implements ServerMessage {
    public static final Identifier IDENTIFIER = new Identifier("screenshot", "send");
    public UUID uuid;
    public String screenshot;

    public SendScreenshotMessage() {
    }

    public SendScreenshotMessage(UUID uuid, String screenshot) {
        this.uuid = uuid;
        this.screenshot = screenshot;
    }

    @Override
    public void onReceive(PacketContext context, PacketByteBuf byteBuf) {
        Invoke.server(() -> context.getTaskQueue().execute(() -> {
            CommandOutput output = CommandUtil.getSource(uuid);
            ChatUtils.sendMessage(output, new LiteralTextBuilder(screenshot)
                    .setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, screenshot))
                    .build());
        }));
    }

    @Override
    public Identifier getIdentifier() {
        return IDENTIFIER;
    }
}