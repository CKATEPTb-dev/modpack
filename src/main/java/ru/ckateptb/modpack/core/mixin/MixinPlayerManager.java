package ru.ckateptb.modpack.core.mixin;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.NbtIo;
import net.minecraft.network.ClientConnection;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.PlayerManager;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.WorldSavePath;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import ru.ckateptb.modpack.core.event.PlayerJoinCallback;
import ru.ckateptb.modpack.core.event.PlayerLeaveCallback;
import ru.ckateptb.modpack.core.mixininterface.OfflinePlayer;
import ru.ckateptb.modpack.core.mixininterface.OfflinePlayersHolder;
import ru.ckateptb.modpack.core.player.ServerOfflinePlayer;

import javax.annotation.Nullable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Mixin(PlayerManager.class)
public abstract class MixinPlayerManager implements OfflinePlayersHolder {
    private final List<OfflinePlayer> offlinePlayers = Collections.synchronizedList(new ArrayList<>());
    private final Map<UUID, OfflinePlayer> offlinePlayerMap = new ConcurrentHashMap<>();
    private final Map<String, OfflinePlayer> offlinePlayerMapByName = new ConcurrentHashMap<>();
    @Shadow
    @Final
    private MinecraftServer server;

    @Override
    public List<OfflinePlayer> getOfflinePlayerList() {
        return this.offlinePlayers;
    }

    @Nullable
    @Override
    public OfflinePlayer getOfflinePlayer(UUID uuid) {
        return this.offlinePlayerMap.get(uuid);
    }

    @Nullable
    @Override
    public OfflinePlayer getOfflinePlayer(String name) {
        return this.offlinePlayerMapByName.get(name);
    }

    @Override
    public void loadFromPlayerDataFolder() throws IOException {
        Path playerDataFolder = server.getSavePath(WorldSavePath.PLAYERDATA);
        if (Files.exists(playerDataFolder)) {
            File[] playerFiles = playerDataFolder.toFile().listFiles();
            if (playerFiles != null) {
                for (File playerFile : playerFiles) {
                    if (playerFile.getName().endsWith(".dat")) {
                        UUID uuid = UUID.fromString(playerFile.getName().substring(0, 36));
                        try (InputStream stream = new FileInputStream(playerFile)) {
                            CompoundTag compoundTag = NbtIo.readCompressed(stream);
                            String username = compoundTag.getString("username");
                            ServerOfflinePlayer player = new ServerOfflinePlayer(uuid, username);
                            offlinePlayers.add(player);
                            offlinePlayerMap.put(uuid, player);
                            offlinePlayerMapByName.put(username, player);
                        }
                    }
                }
            }
        }
    }

    @Inject(method = "onPlayerConnect", at = @At("HEAD"))
    private void onPlayerConnect(ClientConnection connection, ServerPlayerEntity player, CallbackInfo ci) {
        PlayerJoinCallback.EVENT.invoker().onJoin(connection, player);
        UUID uuid = player.getUuid();
        if (getOfflinePlayer(uuid) == null) {
            String username = player.getEntityName();
            ServerOfflinePlayer offlinePlayer = new ServerOfflinePlayer(uuid, username);
            offlinePlayers.add(offlinePlayer);
            offlinePlayerMap.put(uuid, offlinePlayer);
            offlinePlayerMapByName.put(username, offlinePlayer);
        }
    }

    @Inject(method = "remove", at = @At("HEAD"))
    private void remove(ServerPlayerEntity player, CallbackInfo ci) {
        PlayerLeaveCallback.EVENT.invoker().onLeave(player);
    }
}
