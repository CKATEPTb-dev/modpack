package ru.ckateptb.modpack.core.permission.interfaces;

import java.util.Set;

public interface PlayerPermissionHolder extends PermissionHolder {
    void addGroup(GroupPermissionHolder group);

    void removeGroup(GroupPermissionHolder group);

    Set<GroupPermissionHolder> getGroups();

}
