package ru.ckateptb.modpack.screenshot.util;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.FileEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.io.IOException;

public class ImgurUtil {
    public static final ContentType IMAGE_PNG = ContentType.create("image/png");
    public static final ContentType UTF_8 = ContentType.create("text/plain", Consts.UTF_8);

    private static final Gson gson = new Gson();
    private static final CloseableHttpClient client = HttpClients.custom()
            .setUserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) " +
                    "AppleWebKit/537.36 (KHTML, like Gecko) " +
                    "Chrome/83.0.4103.109 Safari/537.36"
            ).build();
    private static final ResponseHandler<String> defaultResponseHandler = response -> {
        int status = response.getStatusLine().getStatusCode();
        if (status >= 200 && status < 300) {
            HttpEntity entity = response.getEntity();
            return entity != null ? EntityUtils.toString(entity) : null;
        } else {
            throw new ClientProtocolException("Unexpected response status: " + status);
        }
    };

    public static String uploadToImgur(File image, String client_id) throws IOException {
        HttpPost post = new HttpPost("https://api.imgur.com/3/image?client_id=" + client_id);
        post.setHeader("accept", "application/json");
        post.setHeader("accept-language", "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7");
        post.setHeader("content-type", "application/json");
        post.setHeader("sec-fetch-dest", "empty");
        post.setHeader("sec-fetch-mode", "cors");
        post.setHeader("sec-fetch-site", "same-site");
        post.setEntity(new FileEntity(image, IMAGE_PNG));
        JsonObject json = gson.fromJson(client.execute(post, defaultResponseHandler), JsonObject.class);
        return json.getAsJsonObject("data").get("link").getAsString();
    }

    public static String uploadToImgur(String imageInBase64, String client_id) throws IOException {
        HttpPost post = new HttpPost("https://api.imgur.com/3/image?client_id=" + client_id);
        post.setHeader("accept", "application/json");
        post.setHeader("accept-language", "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7");
        post.setHeader("content-type", "application/json");
        post.setHeader("sec-fetch-dest", "empty");
        post.setHeader("sec-fetch-mode", "cors");
        post.setHeader("sec-fetch-site", "same-site");
        post.setEntity(new StringEntity(imageInBase64, UTF_8));
        JsonObject json = gson.fromJson(client.execute(post, defaultResponseHandler), JsonObject.class);
        return json.getAsJsonObject("data").get("link").getAsString();
    }
}
