package ru.ckateptb.modpack.core.command;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.builder.ArgumentBuilder;
import net.minecraft.server.command.ServerCommandSource;

public interface CommandArgumentBuilder<S, T> {

    CommandArgumentBuilder<S, T> setPermission(String permission);

    CommandArgumentBuilder<S, T> addChild(CommandArgumentBuilder<?, ?> child);

    CommandArgumentBuilder<S, T> setExecutor(Command<ServerCommandSource> executor);

    ArgumentBuilder<ServerCommandSource, ?> buildThen();
}
