package ru.ckateptb.modpack.core.mixin;

import com.google.gson.*;
import com.mojang.authlib.GameProfile;
import net.minecraft.util.UserCache;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.reflect.Type;
import java.util.Date;
import java.util.UUID;

@Mixin(UserCache.JsonConverter.class)
public abstract class MixinUserCacheJsonConverter {

    private static final MethodHandle CONSTRUCTOR;

    static {
        try {
            CONSTRUCTOR = MethodHandles.publicLookup()
                    .findConstructor(UserCache.Entry.class,
                            MethodType.methodType(void.class,
                                    UserCache.class, GameProfile.class, Date.class));
        } catch (Throwable e) {
            throw new ExceptionInInitializerError(e);
        }
    }

    /**
     * @author CKATEPTb
     * @reason
     */
    @Overwrite
    public JsonElement serialize(UserCache.Entry entry, Type type, JsonSerializationContext jsonSerializationContext) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("name", entry.getProfile().getName());
        UUID uUID = entry.getProfile().getId();
        jsonObject.addProperty("uuid", uUID == null ? "" : uUID.toString());
        return jsonObject;
    }

    /**
     * @author CKATEPTb
     * @reason
     */
    @Overwrite
    public UserCache.Entry deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        if (jsonElement.isJsonObject()) {
            JsonObject jsonObject = jsonElement.getAsJsonObject();
            JsonElement jsonElement2 = jsonObject.get("name");
            JsonElement jsonElement3 = jsonObject.get("uuid");
            if (jsonElement2 != null && jsonElement3 != null) {
                String string = jsonElement3.getAsString();
                String string2 = jsonElement2.getAsString();
                if (string2 != null && string != null) {
                    UUID uUID2 = UUID.fromString(string);
                    try {
                        return (UserCache.Entry) CONSTRUCTOR.invoke(null, new GameProfile(uUID2, string2), null);
                    } catch (Throwable e) {
                        throw new RuntimeException("Exception on instancing", e);
                        //return null;
                    }
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}
